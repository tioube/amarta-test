// /// <reference types="cypress" />

context('dashboard', () => {
    before(() => {
      cy.visit('https://saucedemo.com')
      cy.get('[data-test=username]').click()
      .type('standard_user')
      cy.get('[data-test=password]').click()
      .type('secret_sauce')
      cy.get('[data-test=login-button]').click()
    })
// })

// describe('Login', () => {
    it('Check Dashboard Page', () => {
        cy.get('.title')
        .should('have.text','Products')
      
    //   cy.contains('type').click()
    })

    it('Check the Filter List', () => {
        
        cy.get('.active_option').should('have.text','Name (A to Z)')
        cy.get('[data-test=product_sort_container]')
        .select('za')
        cy.get('.active_option').should('have.text','Name (Z to A)')
        cy.get('[data-test=product_sort_container]')
        .select('hilo')
        cy.get('.active_option').should('have.text','Price (high to low)')
        cy.get('[data-test=product_sort_container]')
        .select('lohi')
        cy.get('.active_option').should('have.text','Price (low to high)')
      //   cy.contains('type').click()
      })
    

  })
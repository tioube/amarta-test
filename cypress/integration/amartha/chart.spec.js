// /// <reference types="cypress" />

context('dashboard', () => {
    before(() => {
      cy.visit('https://saucedemo.com')
      cy.get('[data-test=username]').click()
      .type('standard_user')
      cy.get('[data-test=password]').click()
      .type('secret_sauce')
      cy.get('[data-test=login-button]').click()
    })

    it('Select Item', () => {
        cy.get('#item_4_title_link > .inventory_item_name')
        .click()
    //   cy.contains('type').click()
    })

    it('Check the Item Name', () => {
        cy.get('.inventory_details_name')
        .should('exist')
      })
      it('Check the Item Price', () => {
        cy.get('.inventory_details_price')
        .should('exist')
      })


    it('Add To Chart', () => {
        cy.get('[data-test=add-to-cart-sauce-labs-backpack]').click()
        cy.wait(1000)
    })

    it('Check on Cart ', () => {
        cy.get('.shopping_cart_link').click()
        cy.wait(1000)
        cy.get('[data-test=username]').click()
      .type('standard_user')
      cy.get('[data-test=password]').click()
      .type('secret_sauce')
      cy.get('[data-test=login-button]').click()
      cy.get('[data-test=add-to-cart-sauce-labs-backpack]')
        .click()
        cy.get('.shopping_cart_link').click()
        cy.get('.inventory_item_desc').should('exist')
        cy.get('.inventory_item_price').should('exist')
        cy.get('.inventory_item_price').should('exist')
        cy.get('.cart_quantity').should('exist')
    })

    it('Checkout Page', () => {
        cy.get('[data-test=checkout]').click()
        cy.wait(1000)
        cy.get('[data-test=username]').click()
        .type('standard_user')
        cy.get('[data-test=password]').click()
        .type('secret_sauce')
        cy.get('[data-test=login-button]').click()
        cy.get('[data-test=add-to-cart-sauce-labs-backpack]')
        .click()
        cy.get('.shopping_cart_link').click()
        cy.get('[data-test=checkout]').click()
        cy.get('[data-test=firstName]').type('test')
        cy.get('[data-test=lastName]').type('checkout')
        cy.get('[data-test=postalCode]').type('12922')
        cy.get('[data-test=continue]').click()
    })

    it('Checkout Overview - Pay Info', () => {
        cy.get('.summary_info > :nth-child(2)').should('exist')
        cy.get('.summary_info > :nth-child(4)').should('exist')
        cy.get('.summary_info > :nth-child(4)').should('exist')
        cy.get('.summary_info > :nth-child(4)').should('exist')
        cy.get('.summary_info > :nth-child(4)').should('exist')
        // cy.get('[data-test=finish]').click()
    })

  })
